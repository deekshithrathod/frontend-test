import { functionTypeAnnotation } from "@babel/types";
import React from "react";

const Rest = (props) => {
  console.log(props);
  return (
    <div className="container">
      <h3>
        {props.allData.name}
        <sup> ${props.allData.price}</sup>
      </h3>
      <p>{props.allData.desc}</p>
      <hr />
    </div>
  );
};

export default Rest;
