import { element } from "prop-types";
import React, { Component } from "react";
import LoadingIndicator from "../LoadingIndicator";
import Rest from "../Restaurant/Rest";
const restArr = [
  {
    name: "e47",
    id: 1,
    price: 352,
    desc: "desc",
  },
  {
    name: "a",
    id: 1,
    price: 302,
    desc: "desc",
  },
  {
    name: "n",
    id: 1,
    price: 312,
    desc: "desc",
  },
  {
    name: "m",
    id: 1,
    price: 3872,
    desc: "desc",
  },
  {
    name: "name11",
    id: 1,
    price: 3562,
    desc: "desc",
  },
  {
    name: "name1",
    id: 1,
    price: 31,
    desc: "desc",
  },
  {
    name: "name12",
    id: 1,
    price: 2,
    desc: "desc",
  },
  {
    name: "name3",
    id: 1,
    price: 3,
    desc: "desc",
  },
  {
    name: "name674",
    id: 1,
    price: 324,
    desc: "de4sc",
  },
  {
    name: "name5",
    id: 1,
    price: 326,
    desc: "desc",
  },
  {
    name: "name6",
    id: 1,
    price: 329,
    desc: "desc",
  },
];

class Restaurants extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      nameSort: false,
      priceSort: false,
      restData: restArr.slice(1, 10),
    };
    this.loadConverter = this.loadConverter.bind(this);
    this.theSorterPrice = this.theSorterPrice.bind(this);
    this.theSorterName = this.theSorterName.bind(this);
  }
  loadConverter() {
    this.setState((prevState) => {
      return {
        isLoading: !prevState.isLoading,
      };
    });
  }
  componentDidMount() {
    setTimeout(() => {
      this.loadConverter();
    }, 2000);
  }
  theSorterPrice() {
    const sortedArr = this.state.restData.sort((a, b) => {
      if (!this.state.priceSort) return a.price - b.price;
      return b.price - a.price;
    });
    this.setState((prevState) => {
      return {
        priceSort: !prevState.priceSort,
        restData: sortedArr,
      };
    });
  }
  theSorterName() {
    let sortNameArr = [];
    if (this.state.nameSort) {
      sortNameArr = this.state.restData.sort();
    } else {
      sortNameArr = this.state.restData.sort().reverse();
    }
    this.setState((prevState) => {
      return {
        nameSort: !prevState.nameSort,
        restData: sortNameArr,
      };
    });
  }
  render() {
    if (!this.state.isLoading) {
      return <LoadingIndicator />;
    }
    // const newRestEls = this.state.restData.map((rest) => {
    //   return <Rest key={rest.name} allData={rest} />;
    // });
    return (
      <>
        <div>
          <button onClick={this.theSorterName}>Sort (name)</button>
          <button onClick={this.theSorterPrice}>Sort (price)</button>
        </div>
        {this.state.restData.map((rest) => {
          return <Rest key={rest.name} allData={rest} />;
        })}
      </>
    );
  }
}

export default Restaurants;
