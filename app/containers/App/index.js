/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { Component, Fragment } from "react";
import { Helmet } from "react-helmet";
import styled from "styled-components";
import { Switch, Route } from "react-router-dom";
// import React from "react";
import HomePage from "containers/HomePage/Loadable";

import GlobalStyle from "../../global-styles";

function App() {
  return (
    <Fragment>
      <Helmet titleTemplate="%s - Calance" defaultTitle="Calance">
        <meta name="description" content="Calance" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={HomePage} />
      </Switch>
      <GlobalStyle />
    </Fragment>
  );
}

export default App;
